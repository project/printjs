(function ($, Drupal, drupalSettings, once) {

  'use strict';

  Drupal.behaviors.printjs = {
    attach: function (context, settings) {
      let eventClick = '.btn-print';
      let printSelector = 'print';
      if (drupalSettings.hasOwnProperty('printjs')) {
        if (drupalSettings.printjs.btn_selector_print) {
          eventClick += ' ,' + drupalSettings.printjs.btn_selector_print;
        }
        let print_parent_selector = drupalSettings.printjs.print_parent_selector;
        // Check string is class selector, or id or simple text
        if (drupalSettings.printjs.printjs_id && drupalSettings.printjs.printjs_id.trim() !== "") {
          let selector = drupalSettings.printjs.printjs_id.trim();
          let $element = $(selector);
          if ($element && selector.startsWith(".")) {
            if (print_parent_selector) {
              $element.parent().attr('id', 'wrapper-print');
            } else {
              $element.attr('id', 'print');
            }
          } else if ($element && selector.startsWith("#")) {
            if (print_parent_selector) {
              $element.parent().attr('id', 'wrapper-print');
            }
          } else if ($element) {
            let $element = $(selector).first();
            if ($element.length) {
              if (!$element.attr('id')) {
                $element.attr('id', 'print');
                if (print_parent_selector) {
                  $element.parent().attr('id', 'wrapper-print');
                }
              } else {
                if (print_parent_selector) {
                  $element.parent().attr('id', 'wrapper-print');
                } else {
                  $element.wrap('<div class="wrapper-print" id="wrapper-print"></div>');
                }
              }
            }
          }
        }
      }
      $(once('printjs', eventClick, context)).each(function () {
        init(eventClick, printSelector);
        let data = $(this).data();
        if (data.autoprint && parseInt(data.autoprint) == 1) {
          $(this).click();
          return;
        }
      });

      function init(eventClick, printSelector) {

        // Check event onclick.
        $(eventClick).on('click', function () {
          let css = [];
          let config = $(this).data();
          $("head").find("link[rel='stylesheet']").attr("href", function (i, value) {
            css.push(value);
          });
          config.css = css;
          if (printSelector.length) {
            config.printable = printSelector;
          }
          if (!config.type.length) {
            config.type = 'html';
          }

          if (!$('#' + config.printable).length) {
            if ($('main').length) {
              $('main').attr('id', config.printable);
            } else {
              $('body').attr('id', config.printable);
            }
          }
          // Print parent #print.
          if (drupalSettings.hasOwnProperty('printjs') && drupalSettings.printjs.print_parent_selector) {
            config.printable = "wrapper-print";
          }

          if (config.printable) {
            printJS(config);
          } else {
            window.print();
          }
        });
      }
    }
  }
}(jQuery, Drupal, drupalSettings, once));
