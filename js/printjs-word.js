(function ($, Drupal, drupalSettings, once) {
  'use strict';

  Drupal.behaviors.printjsWord = {
    attach: function (context, settings) {
      once('printjs-word', '.btn-print', context).forEach(function (element) {
        $(element).on('click', function () {
          let pageTitle = document.title;
          let printId = $(this).data('printable');
          let parent = $(this).data('print_parent');
          if(printId == '') {
            printId = '#print';
          }
          let content =  $(printId).html();

          var cssUrls = [];
          $("head").find("link[rel='stylesheet']").each(function (i, el) {
            var href = $(el).attr("href");
            if (href) {
              cssUrls.push(href);
            }
          });

          var inlineCss = "";
          $("head").find("style").each(function (i, el) {
            inlineCss += $(el).html() + "\n";
          });

          Promise.all(
            cssUrls.map(function (url) {
              return fetch(url).then(function (response) {
                return response.text();
              });
            })
          )
          .then(function (cssContents) {
            let combinedCss = cssContents.join("\n") + "\n" + inlineCss;
            let html = `<html>
              <head>
                <meta charset="utf-8">
                <title>${pageTitle}</title>
                <style>
                  ${combinedCss}
                </style>
              </head>
              <body>
                ${content}
              </body>
            </html>`;
            // let inlinedHtml = juice(html);
            let blob = htmlDocx.asBlob(html);
            let url = URL.createObjectURL(blob);
            let downloadLink = document.createElement("a");
            downloadLink.href = url;
            downloadLink.download = pageTitle + ".docx";
            document.body.appendChild(downloadLink);
            downloadLink.click();
            document.body.removeChild(downloadLink);
          })
          .catch(function (error) {
            console.error("Error fetching CSS files:", error);
          });
        });
      });
    }
  };
}(jQuery, Drupal, drupalSettings, once));
