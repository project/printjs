# About
The Drupal module "PrintJS" is designed to facilitate the printing
of content within a specified HTML div element.
This module utilizes the "PrintJS" library (https://printjs.crabbly.com/)
to achieve this functionality. By default, it targets a div element
with the id="print" but can be configured to print content
from other specified divs.

### How to use module,
- You should create the content you want to print within an HTML div id='print'
- Add Printjs block, It will display a print button, when clicked,
will send the content within the "print" div, along with the page's CSS,
to the printer.
- you can add print button in header or footer of views
- You can customize the behavior of the print button. It can be configured
to print content in different formats such as PDF, image, JSON, etc.,
similar to the options available at https://printjs.crabbly.com/ by using
the function hook_preprocess_printjs()
