<?php

namespace Drupal\printjs;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Service to get button printjs.
 */
class Printjs {

  use StringTranslationTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The Printjs constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   Injected service.
   */
  public function __construct(ConfigFactoryInterface $configFactory, TranslationInterface $string_translation) {
    $this->configFactory = $configFactory;
    $this->stringTranslation = $string_translation;
  }

  /**
   * Method get button print js.
   *
   * {@inheritDoc}
   */
  public function getBtnPrintjs($printText = "Print", $config = []) {
    $configuration = [];
    if (empty($config)) {
      $config = $this->configFactory->get('printjs.settings');
    }
    if (is_object($config)) {
      $configuration = $config->getRawData();
    }
    elseif (is_array($config)) {
      $configuration = $config;
    }
    $printable = $configuration["printjs_id"] ?? 'print';
    $attributes = [
      'class' => [
        'btn-print',
        'btn',
        'btn-success',
      ],
      'data-type' => 'html',
      'data-printable' => $printable,
      'data-autoprint' => $configuration["auto_print"] ?? FALSE,
      'data-print_parent' => $configuration["print_parent_selector"] ?? FALSE,
    ];
    $lib = !empty($configuration['local']) ? 'printjs/printjs.local' : 'printjs/printjs';
    if ($configuration['word']) {
      $lib = 'printjs/printjs.word';
    }
    return [
      '#theme' => 'printjs',
      '#printText' => $this->t('@printtext', ['@printtext' => $printText]),
      '#attributes' => $attributes,
      '#attached' => [
        'library' => [$lib],
        'drupalSettings' => ['printjs' => $configuration],
      ],
    ];
  }

}
