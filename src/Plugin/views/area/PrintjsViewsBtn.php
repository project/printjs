<?php

namespace Drupal\printjs\Plugin\views\area;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Attribute\ViewsArea;
use Drupal\views\Plugin\views\area\TokenizeAreaPluginBase;

/**
 * Views area handler for a Printjs button.
 *
 * @ingroup views_area_handlers
 */
#[ViewsArea("printjs_views_btn")]
class PrintjsViewsBtn extends TokenizeAreaPluginBase {

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['printjs_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom selector for print content'),
      '#description' => implode("\n", [
        $this->t('By default it will take #print, but you can custom your selector ex: .view-id-@view_id', ['@view_id' => $this->view->id()]),
        $this->t('View more <a href="@exemple">printjs.crabbly.com</a>', ['@exemple' => 'https://printjs.crabbly.com/']),
      ]),
      '#default_value' => $this->options['printjs_id'],
    ];
    $form['printText'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button text'),
      '#required' => TRUE,
      '#default_value' => $this->options['printText'],
    ];
    $form['auto_print'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Auto print'),
      '#description' => $this->t('Automatically click the Print button when the page loads'),
      '#default_value' => $this->options['auto_print'],
    ];
    $form['print_parent_selector'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Print parent selector'),
      '#default_value' => $this->options['print_parent_selector'],
    ];
    $form['local'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use libraries/Print.js/print.min.js'),
      '#description' => $this->t('Download at <a href="@printjs">Print.js</a>', ['@printjs' => 'https://github.com/crabbly/Print.js/releases/']),
      '#default_value' => $this->options['local'],
    ];
    $form['word'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Export to word'),
      '#description' => $this->t('In some cases where editing in Word is required before printing, when pressing the button it will download the docx file instead of sending to printer directly.'),
      '#default_value' => $this->options['word'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    $config = $this->options;
    $type = $this->areaType;
    if (!empty($this->view->$type)) {
      $config = $this->view->$type["area_printjs_views"]->options;
    }
    /* @phpstan-ignore-next-line */
    return \Drupal::service('print.js')->getBtnPrintjs($this->options['printText'], $config);
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    /* @phpstan-ignore-next-line */
    $config = \Drupal::config('printjs.settings');
    $printJsId = $config->get('printjs_id');
    $printParentSelector = $config->get('print_parent_selector');
    $local = $config->get('local');
    $options = parent::defineOptions();
    $auto_print = $config->get('auto_print');
    $options['auto_print'] = ['default' => $auto_print];
    $options['printText'] = ['default' => $this->t('Print')];
    $options['print_parent_selector'] = ['default' => $printParentSelector];
    $options['printjs_id'] = ['default' => $printJsId];
    $options['local'] = ['default' => $local];
    $options['word'] = ['default' => $config->get('word')];
    return $options;
  }

}
