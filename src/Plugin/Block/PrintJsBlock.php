<?php

namespace Drupal\printjs\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\printjs\Printjs;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block with a simple print button.
 */
#[Block(
  id: "printjs_block",
  admin_label: new TranslatableMarkup("Print button"),
  category: new TranslatableMarkup("Print")
)]
class PrintJsBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The Print Js service.
   *
   * @var \Drupal\printjs\Printjs
   */
  protected $printJs;

  /**
   * Creates a new block for Print button.
   *
   * @param array $configuration
   *   An associative array containing the plugin's configuration.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\printjs\Printjs $print_js
   *   The Print Js service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Printjs $print_js) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->printJs = $print_js;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('print.js')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = $this->getConfiguration();
    return [
      'printjs_id' => $config['printjs_id'] ?? '#print',
      'print_parent_selector' => FALSE,
      'auto_print' => FALSE,
      'printText' => $this->t('Print'),
      'local' => FALSE,
      'word' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    return $this->printJs->getBtnPrintjs($config['printText'], $config);
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $form['printjs_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom selector for print content'),
      '#description' => $this->t('By default, it will take #print, but you can set your selector ex: .class-selector, View more <a href="@exemple">printjs.crabbly.com</a>', ['@exemple' => 'https://printjs.crabbly.com/']),
      '#default_value' => $this->configuration['printjs_id'],
    ];
    $form['printText'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button text'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['printText'],
    ];
    $form['auto_print'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Auto print'),
      '#description' => $this->t('Automatically click the Print button when the page loads'),
      '#default_value' => $this->configuration['auto_print'],
    ];
    $form['print_parent_selector'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Print parent selector'),
      '#default_value' => $this->configuration['print_parent_selector'],
    ];
    $form['local'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use libraries/Print.js/print.min.js'),
      '#description' => $this->t('Download at <a href="@printjs">Print.js</a>', ['@printjs' => 'https://github.com/crabbly/Print.js/releases/']),
      '#default_value' => $this->configuration['local'],
    ];
    $form['word'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Export to word'),
      '#description' => $this->t('In some cases where editing in Word is required before printing, when pressing the button it will download the docx file instead of sending to printer directly.'),
      '#default_value' => $this->configuration['word'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $this->setConfigurationValue('printText', $form_state->getValue('printText'));
    $this->setConfigurationValue('printjs_id', $form_state->getValue('printjs_id'));
    $this->setConfigurationValue('print_parent_selector', $form_state->getValue('print_parent_selector'));
    $this->setConfigurationValue('local', $form_state->getValue('local'));
    $this->setConfigurationValue('word', $form_state->getValue('word'));
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

}
