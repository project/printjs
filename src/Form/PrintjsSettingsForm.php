<?php

namespace Drupal\printjs\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class PrintjsSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'printjs.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'printjs_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['printjs_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom selector for print content'),
      '#description' => $this->t('By default it will take #print, but you can custom id') . $this->t('View more <a href="@exemple">printjs.crabbly.com</a>', ['@exemple' => 'https://printjs.crabbly.com/']),
      '#default_value' => $config->get('printjs_id'),
    ];

    $form['btn_selector_print'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Selector for button print'),
      '#description' => $this->t('By default it will catch event on class .btn-print <br/>You can add more selector, Separate by , example : #printJS-form, #printjs'),
      '#default_value' => $config->get('btn_selector_print'),
    ];

    $form['print_parent_selector'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Print parent selector'),
      '#default_value' => $config->get('print_parent_selector'),
    ];

    $form['local'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use libraries/Print.js/print.min.js'),
      '#description' => $this->t('Download at <a href="@printjs">Print.js</a>', ['@printjs' => 'https://github.com/crabbly/Print.js/releases/']),
      '#default_value' => $config->get('local'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set('printjs_id', $form_state->getValue('printjs_id'))
      ->set('btn_selector_print', $form_state->getValue('btn_selector_print'))
      ->set('print_parent_selector', $form_state->getValue('print_parent_selector'))
      ->set('local', $form_state->getValue('local'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
